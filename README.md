# wabbit-rs

A rust port of my wabbit compiler written as part of [this compiler course](https://www.dabeaz.com/compiler.html).

Components:

- [model](./src/wabbit/model.rs): more or less the definition of the AST nodes
- [interpreter](./src/wabbit/interpreter.rs): a definitional interpreter for the AST
