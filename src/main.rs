// early-stage transformation of the Wabbit compiler I wrote in Python
// during david beazley's compilers course
mod wabbit;
use wabbit::interpreter::{interpret_expr, interpret_program, ProgramState};
use wabbit::model::*;

fn main() {
    let source_1 = String::from("2 + 3 * 4");
    let mul_op: Expression = Expression::BinOp(BinOp {
        op: "*",
        left: Box::new(Expression::Literal(Literal::Integer(3))),
        right: Box::new(Expression::Literal(Literal::Integer(4))),
    });
    let model_1 = Expression::BinOp(BinOp {
        op: "+",
        left: Box::new(Expression::Literal(Literal::Integer(2))),
        right: Box::new(mul_op),
    });
    println!("Hello, world!");
    println!(
        "model: {:?}",
        expr_to_source(Expression::Literal(Literal::Integer(4)))
    );
    println!("source: {:?}", source_1);
    println!("model: {:?}", expr_to_source(model_1));
    let print_stmts = Statements {
        statements: vec![Statement::Print(Print {
            expr: Expression::Literal(Literal::Integer(4)),
        })],
    };
    interpret_program(print_stmts);

    interpret_simple_add();
    interpret_simple_sub();
    interpret_simple_mul();
}

fn interpret_simple_add() {
    let add: Expression = Expression::BinOp(BinOp {
        op: "+",
        left: Box::new(Expression::Literal(Literal::Integer(3))),
        right: Box::new(Expression::Literal(Literal::Integer(4))),
    });

    println!("{:?}", interpret_expr(add, ProgramState::new()).result);
}

fn interpret_simple_sub() {
    let sub: Expression = Expression::BinOp(BinOp {
        op: "-",
        left: Box::new(Expression::Literal(Literal::Integer(3))),
        right: Box::new(Expression::Literal(Literal::Integer(4))),
    });

    println!(
        "{:?} => {:?}",
        expr_to_source(sub.clone()),
        interpret_expr(sub, ProgramState::new()).result
    );
}

fn interpret_simple_mul() {
    let mul: Expression = Expression::BinOp(BinOp {
        op: "*",
        left: Box::new(Expression::Literal(Literal::Integer(3))),
        right: Box::new(Expression::Literal(Literal::Integer(4))),
    });

    println!(
        "{:?} => {:?}",
        expr_to_source(mul.clone()),
        interpret_expr(mul, ProgramState::new()).result
    );
}
