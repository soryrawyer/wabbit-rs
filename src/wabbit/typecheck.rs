/// typechecking-related structs, functions, things of that nature
use std::fmt;

#[derive(Debug, Clone)]
pub struct TypeError;

impl fmt::Display for TypeError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "invalid types")
    }
}
