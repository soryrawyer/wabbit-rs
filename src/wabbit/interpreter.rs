use super::model::{BinOp, Expression, Literal, Statement, Statements};
use super::typecheck::TypeError;
use std::collections::HashMap;

#[derive(Clone)]
pub struct VarValue {
    name: String,
    value: Option<Literal>,
    var_type: String,
}

#[derive(Clone)]
pub struct ProgramState {
    pub variables: HashMap<String, VarValue>,
    pub binops: HashMap<&'static str, BinOpSig>,
}

impl ProgramState {
    pub fn new() -> ProgramState {
        ProgramState {
            variables: HashMap::new(),
            binops: builtin_binops(),
        }
    }
}

pub struct ReturnValue {
    env: ProgramState,
    pub result: Option<Literal>,
}

pub fn interpret_program(model: Statements) -> ReturnValue {
    let state = ProgramState::new();
    interpret_statements(model, state)
}

fn interpret_statements(stmts: Statements, state: ProgramState) -> ReturnValue {
    let mut p: ProgramState = state;
    for stmt in stmts.statements {
        let ret_val = interpret_statement(stmt, p);
        p = ret_val.env;
    }
    ReturnValue {
        env: p,
        result: None,
    }
}

fn interpret_statement(stmt: Statement, state: ProgramState) -> ReturnValue {
    match stmt {
        Statement::Comment { .. } => ReturnValue {
            env: state,
            result: None,
        },
        Statement::Print(p) => {
            let result = interpret_expr(p.expr, state.clone());
            match result.result {
                Some(value) => print!("{:?}", value),
                None => print!("()"),
            }
            ReturnValue {
                env: state,
                result: None,
            }
        }
        _ => ReturnValue {
            env: state,
            result: None,
        },
    }
}

fn interpret_binop(op: BinOp, state: ProgramState) -> ReturnValue {
    if let Some(func) = state.binops.get(op.op) {
        let left = interpret_expr(*op.left, state.clone()).result.unwrap();
        let right = interpret_expr(*op.right, state.clone()).result.unwrap();
        let result = func(left, right);
        if let Ok(lit) = result {
            ReturnValue {
                env: state,
                result: Some(lit),
            }
        } else {
            panic!("ohhhh no non no non o")
        }
    } else {
        panic!("omgomgomg")
    }
}

pub fn interpret_expr(expr: Expression, state: ProgramState) -> ReturnValue {
    match expr {
        Expression::Literal(value) => ReturnValue {
            env: state,
            result: Some(value),
        },
        Expression::BinOp(bo) => interpret_binop(bo, state),
        _ => ReturnValue {
            env: state,
            result: None,
        },
    }
}

type BinOpSig = fn(Literal, Literal) -> Result<Literal, TypeError>;

fn interpret_add(x: Literal, y: Literal) -> Result<Literal, TypeError> {
    match (x, y) {
        (Literal::Integer(a), Literal::Integer(b)) => Ok(Literal::Integer(a + b)),
        (Literal::Float(a), Literal::Float(b)) => Ok(Literal::Float(a + b)),
        _ => Err(TypeError),
    }
}

fn interpret_sub(x: Literal, y: Literal) -> Result<Literal, TypeError> {
    match (x, y) {
        (Literal::Integer(a), Literal::Integer(b)) => Ok(Literal::Integer(a - b)),
        (Literal::Float(a), Literal::Float(b)) => Ok(Literal::Float(a - b)),
        _ => Err(TypeError),
    }
}

fn interpret_mul(x: Literal, y: Literal) -> Result<Literal, TypeError> {
    match (x, y) {
        (Literal::Integer(a), Literal::Integer(b)) => Ok(Literal::Integer(a * b)),
        (Literal::Float(a), Literal::Float(b)) => Ok(Literal::Float(a * b)),
        _ => Err(TypeError),
    }
}

fn interpret_div(x: Literal, y: Literal) -> Result<Literal, TypeError> {
    match (x, y) {
        (Literal::Integer(a), Literal::Integer(b)) => Ok(Literal::Integer(a / b)),
        (Literal::Float(a), Literal::Float(b)) => Ok(Literal::Float(a / b)),
        _ => Err(TypeError),
    }
}

/// this is a starting point for binary operations supported by Wabbit
/// this needs to be a function, and not a global constant, because HashMap
/// can't be part of a constant?
fn builtin_binops() -> HashMap<&'static str, BinOpSig> {
    let mut map = HashMap::new();
    map.insert("+", interpret_add as BinOpSig);
    map.insert("-", interpret_sub as BinOpSig);
    map.insert("*", interpret_mul as BinOpSig);
    map.insert("/", interpret_div as BinOpSig);
    map
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn eval_literal() {
        let i: u64 = 4;
        let int = Literal::Integer(4);
        let result = interpret_expr(Expression::Literal(int), ProgramState::new());
        match result.result {
            Some(Literal::Integer(value)) => assert_eq!(value, i),
            _ => assert_eq!(true, false),
        }
    }

    #[test]
    fn eval_add_binop() {
        // test that 3 + 4 = 7
        let add: Expression = Expression::BinOp(BinOp {
            op: "+",
            left: Box::new(Expression::Literal(Literal::Integer(3))),
            right: Box::new(Expression::Literal(Literal::Integer(4))),
        });

        let result = interpret_expr(add, ProgramState::new()).result;
        match result {
            Some(Literal::Integer(i)) => assert_eq!(i, 7),
            _ => assert_eq!(true, false),
        }
    }
}
