// model.rs
// a file for things related to the model of Wabbit

use std::fmt;

#[derive(Debug, Clone)]
pub struct UnaryOp<'a> {
    pub op: &'a str,
    pub expr: Box<Expression<'a>>,
}

#[derive(Debug, Clone)]
pub struct BinOp<'a> {
    pub op: &'a str,
    pub left: Box<Expression<'a>>,
    pub right: Box<Expression<'a>>,
}

#[derive(Debug, Clone)]
pub struct BoolOp<'a> {
    op: &'a str,
    left: Box<Expression<'a>>,
    right: Box<Expression<'a>>,
}

#[derive(Debug, Clone)]
pub struct CompExpr<'a> {
    expr_stmt: ExprStmt<'a>,
    statements: Option<Statements<'a>>,
}

#[derive(Debug, Clone)]
pub enum Expression<'a> {
    Literal(Literal),
    UnaryOp(UnaryOp<'a>),
    BinOp(BinOp<'a>),
    BoolOp(BoolOp<'a>),
    NameExpr(Name),
    Load(Name),
    CompoundExpression(CompExpr<'a>),
}

pub type Integer = i64;
pub type Float = f64;

#[derive(Debug, Clone)]
pub enum Literal {
    Integer(Integer),
    Float(Float),
    Char(char),
    Unit,
    BreakResult,
    ContinueResult,
    Boolean(bool),
}

impl fmt::Display for Literal {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Literal::Boolean(b) => write!(f, "{:?}", b),
            Literal::Integer(i) => write!(f, "{:?}", i),
            Literal::Float(fl) => write!(f, "{:?}", fl),
            Literal::Char(c) => write!(f, "{:?}", c),
            Literal::Unit => write!(f, "()"),
            _ => write!(f, ""),
        }
    }
}

#[derive(Debug, Clone)]
pub struct Name {
    name: String,
}

#[derive(Debug, Clone)]
pub struct Comment {
    content: String,
    is_multiline: bool,
}

#[derive(Debug, Clone)]
pub struct Print<'a> {
    pub expr: Expression<'a>,
}

#[derive(Debug, Clone)]
pub struct Assign<'a> {
    destination: Name,
    expr: Expression<'a>,
}

#[derive(Debug, Clone)]
pub struct Return<'a> {
    expr: Expression<'a>,
}

#[derive(Debug, Clone)]
pub struct VarDecl<'a> {
    var_type: String,
    name: Name,
    datatype: Option<String>,
    initial_value: Option<Expression<'a>>,
}

#[derive(Debug, Clone)]
pub struct IfStmt<'a> {
    test: Expression<'a>,
    consequence: Vec<Box<Statement<'a>>>,
    alternative: Option<Vec<Box<Statement<'a>>>>,
}

#[derive(Debug, Clone)]
pub struct WhileStmt<'a> {
    test: Expression<'a>,
    body: Vec<Box<Statement<'a>>>,
}

#[derive(Debug, Clone)]
pub struct ExprStmt<'a> {
    expr: Box<Expression<'a>>,
}

#[derive(Debug, Clone)]
pub enum Statement<'a> {
    Comment(Comment),
    Print(Print<'a>),
    Assign(Assign<'a>),
    Return(Return<'a>),
    VarDecl(VarDecl<'a>),
    If(IfStmt<'a>),
    While(WhileStmt<'a>),
    Break,
    Continue,
    ExpressionStatement(ExprStmt<'a>),
}

#[derive(Debug, Clone)]
pub struct Statements<'a> {
    pub statements: Vec<Statement<'a>>,
}

pub fn expr_to_source(expr: Expression) -> String {
    match expr {
        Expression::Literal(value) => value.to_string(),
        Expression::BinOp(b) => {
            let left_str = expr_to_source(*b.left);
            let right_str = expr_to_source(*b.right);
            format!("{} {} {}", left_str, b.op, right_str)
        }
        Expression::UnaryOp(u) => {
            format!("{}{}", u.op, expr_to_source(*u.expr))
        }
        Expression::NameExpr(name) => name.name,
        Expression::Load(name) => name.name,
        _ => String::from(""),
    }
}

#[derive(Copy, Clone)]
struct PrintContext {
    indent_level: usize,
}

fn stmt_to_source(stmt: Statement, context: PrintContext) -> String {
    let source = match stmt {
        Statement::Print(p) => {
            let expr_source = expr_to_source(p.expr);
            format!("print {};", expr_source)
        }
        Statement::Assign(a) => {
            format!(
                "{} = {};",
                expr_to_source(Expression::NameExpr(a.destination)),
                expr_to_source(a.expr)
            )
        }
        Statement::VarDecl(a) => {
            let mut components: Vec<String> = Vec::new();
            components.push(a.var_type);
            components.push(expr_to_source(Expression::NameExpr(a.name)));
            match a.datatype {
                Some(s) => components.push(s),
                // TODO: going over this with "flatMap" might be doable?
                None => (),
            }
            match a.initial_value {
                Some(expr) => {
                    components.push(String::from("="));
                    components.push(expr_to_source(expr));
                }
                None => (),
            }
            format!("{};", components.join(" "))
        }
        Statement::Comment(c) => {
            if c.is_multiline {
                vec![String::from("/*"), c.content, String::from("/")].join("\n")
            } else {
                format!("//{}", c.content)
            }
        }
        Statement::If(i) => if_to_source(i.test, i.consequence, i.alternative, &context),
        Statement::While(w) => {
            let mut lines: Vec<String> = Vec::new();
            lines.push(format!("while {} {{", expr_to_source(w.test)));
            let body_context = PrintContext {
                indent_level: context.indent_level + 4,
            };
            let body_source: Vec<String> = w
                .body
                .into_iter()
                .map(|stmt| stmt_to_source(*stmt, body_context))
                .collect();
            lines.extend(body_source);
            lines.push(String::from("}"));
            lines.join("\n")
        }
        Statement::Break => String::from("break;"),
        Statement::Continue => String::from("continue;"),
        _ => String::from("omgomgomg"),
    };
    format!("{}{}", " ".repeat(context.indent_level), source)
}

fn if_to_source(
    test: Expression,
    consequence: Vec<Box<Statement>>,
    alternative: Option<Vec<Box<Statement>>>,
    context: &PrintContext,
) -> String {
    let mut lines: Vec<String> = Vec::new();
    lines.push(format!("if {} {{", expr_to_source(test)));
    let cons_source: Vec<String> = consequence
        .into_iter()
        .map(|stmt| {
            stmt_to_source(
                *stmt,
                PrintContext {
                    indent_level: context.indent_level + 4,
                },
            )
        })
        .collect();
    lines.extend(cons_source);
    match alternative {
        Some(stmts) => {
            let alt_source: Vec<String> = stmts
                .into_iter()
                .map(|stmt| {
                    stmt_to_source(
                        *stmt,
                        PrintContext {
                            indent_level: context.indent_level + 4,
                        },
                    )
                })
                .collect();
            lines.push(String::from("} else {"));
            lines.extend(alt_source);
            lines.push(String::from("}"));
        }
        None => lines.push(String::from("}")),
    }
    lines.join("\n")
}

pub fn to_source(stmts: Statements) -> String {
    stmts
        .statements
        .into_iter()
        .map(|stmt| stmt_to_source(stmt, PrintContext { indent_level: 0 }))
        .collect::<Vec<String>>()
        .join("\n")
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn int_to_string() {
        assert_eq!(
            expr_to_source(Expression::Literal(Literal::Integer(4))),
            String::from("4")
        );
    }

    #[test]
    fn binop_to_string() {
        let source_1 = String::from("2 + 3 * 4");
        let mul_op: Expression = Expression::BinOp(BinOp {
            op: "*",
            left: Box::new(Expression::Literal(Literal::Integer(3))),
            right: Box::new(Expression::Literal(Literal::Integer(4))),
        });
        let model_1 = Expression::BinOp(BinOp {
            op: "+",
            left: Box::new(Expression::Literal(Literal::Integer(2))),
            right: Box::new(mul_op),
        });
        assert_eq! {expr_to_source(model_1), source_1};
    }

    #[test]
    fn print_statements_to_source() {
        let source = "print 2 + 3 * -4;\n\
                      print 2.0 - 3.0 / -4.0;";
        let print1_model = Statement::Print(Print {
            expr: Expression::BinOp(BinOp {
                op: "+",
                left: Box::new(Expression::Literal(Literal::Integer(2))),
                right: Box::new(Expression::BinOp(BinOp {
                    op: "*",
                    left: Box::new(Expression::Literal(Literal::Integer(3))),
                    right: Box::new(Expression::UnaryOp(UnaryOp {
                        op: "-",
                        expr: Box::new(Expression::Literal(Literal::Integer(4))),
                    })),
                })),
            }),
        });
        let print2_model = Statement::Print(Print {
            expr: Expression::BinOp(BinOp {
                op: "-",
                left: Box::new(Expression::Literal(Literal::Float(2.0))),
                right: Box::new(Expression::BinOp(BinOp {
                    op: "/",
                    left: Box::new(Expression::Literal(Literal::Float(3.0))),
                    right: Box::new(Expression::UnaryOp(UnaryOp {
                        op: "-",
                        expr: Box::new(Expression::Literal(Literal::Float(4.0))),
                    })),
                })),
            }),
        });
        let statements = Statements {
            statements: vec![print1_model, print2_model],
        };
        assert_eq!(to_source(statements), source);
    }

    #[test]
    fn test_statements_to_string() {
        let source = "const pi = 3.14159;\n\
        var tau float;\n\
        tau = 2.0 * pi;\n\
        print -tau + 10.0;";
        let pi_assign = Statement::VarDecl(VarDecl {
            var_type: String::from("const"),
            name: Name {
                name: String::from("pi"),
            },
            datatype: None,
            initial_value: Some(Expression::Literal(Literal::Float(3.14159))),
        });
        let tau_decl = Statement::VarDecl(VarDecl {
            var_type: String::from("var"),
            name: Name {
                name: String::from("tau"),
            },
            datatype: Some(String::from("float")),
            initial_value: None,
        });
        let tau_assign = Statement::Assign(Assign {
            destination: Name {
                name: String::from("tau"),
            },
            expr: Expression::BinOp(BinOp {
                op: "*",
                left: Box::new(Expression::Literal(Literal::Float(2.0))),
                right: Box::new(Expression::Load(Name {
                    name: String::from("pi"),
                })),
            }),
        });
        let print_expr = Statement::Print(Print {
            expr: Expression::BinOp(BinOp {
                op: "+",
                left: Box::new(Expression::UnaryOp(UnaryOp {
                    op: "-",
                    expr: Box::new(Expression::Load(Name {
                        name: String::from("tau"),
                    })),
                })),
                right: Box::new(Expression::Literal(Literal::Float(10.0))),
            }),
        });
        let model = Statements {
            statements: vec![pi_assign, tau_decl, tau_assign, print_expr],
        };
        assert_eq!(to_source(model), source);
    }

    #[test]
    fn test_comment_to_string() {
        let source = String::from("//Each statement below prints \"true\"");
        let model = Statement::Comment(Comment {
            content: String::from("Each statement below prints \"true\""),
            is_multiline: false,
        });
        assert_eq!(
            stmt_to_source(model, PrintContext { indent_level: 0 }),
            source
        );
    }

    #[test]
    fn test_if_to_string() {
        let source = "var a int = 2;\n\
var b int = 3;\n\
if a < b {
    print a;\n\
} else {
    print b;\n\
}";
        let model = Statements {
            statements: vec![
                Statement::VarDecl(VarDecl {
                    var_type: String::from("var"),
                    name: Name {
                        name: String::from("a"),
                    },
                    datatype: Some(String::from("int")),
                    initial_value: Some(Expression::Literal(Literal::Integer(2))),
                }),
                Statement::VarDecl(VarDecl {
                    var_type: String::from("var"),
                    name: Name {
                        name: String::from("b"),
                    },
                    datatype: Some(String::from("int")),
                    initial_value: Some(Expression::Literal(Literal::Integer(3))),
                }),
                Statement::If(IfStmt {
                    test: Expression::BinOp(BinOp {
                        op: "<",
                        left: Box::new(Expression::Load(Name {
                            name: String::from("a"),
                        })),
                        right: Box::new(Expression::Load(Name {
                            name: String::from("b"),
                        })),
                    }),
                    consequence: vec![Box::new(Statement::Print(Print {
                        expr: Expression::Load(Name {
                            name: String::from("a"),
                        }),
                    }))],
                    alternative: Some(vec![Box::new(Statement::Print(Print {
                        expr: Expression::Load(Name {
                            name: String::from("b"),
                        }),
                    }))]),
                }),
            ],
        };
        assert_eq!(to_source(model), source);
    }

    #[test]
    fn while_to_source() {
        let source = "while x < n {
    fact = fact * x;
    x = x + 1;
    print fact;
}";
        let model = Statements {
            statements: vec![Statement::While(WhileStmt {
                test: Expression::BinOp(BinOp {
                    op: "<",
                    left: Box::new(Expression::Load(Name {
                        name: String::from("x"),
                    })),
                    right: Box::new(Expression::Load(Name {
                        name: String::from("n"),
                    })),
                }),
                body: vec![
                    Box::new(Statement::Assign(Assign {
                        destination: Name {
                            name: String::from("fact"),
                        },
                        expr: Expression::BinOp(BinOp {
                            op: "*",
                            left: Box::new(Expression::Load(Name {
                                name: String::from("fact"),
                            })),
                            right: Box::new(Expression::Load(Name {
                                name: String::from("x"),
                            })),
                        }),
                    })),
                    Box::new(Statement::Assign(Assign {
                        destination: Name {
                            name: String::from("x"),
                        },
                        expr: Expression::BinOp(BinOp {
                            op: "+",
                            left: Box::new(Expression::Load(Name {
                                name: String::from("x"),
                            })),
                            right: Box::new(Expression::Literal(Literal::Integer(1))),
                        }),
                    })),
                    Box::new(Statement::Print(Print {
                        expr: Expression::Load(Name {
                            name: String::from("fact"),
                        }),
                    })),
                ],
            })],
        };
        assert_eq!(to_source(model), source);
    }
}
